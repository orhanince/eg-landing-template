
// Preloader
$(window).load(function() { 
  $("#loader").delay(500).fadeOut(); 
  $(".mask").delay(1000).fadeOut("fadeIn");
});



$(document).ready(function(){

  // Show Navbar
  $(window).on('scroll',function(){
    if ( $( window ).scrollTop() > 400 ) {
                $( 'nav.navbar' ).addClass( 'sticky' );
            } else {
                $( 'nav.navbar' ).removeClass( 'sticky' );
            }

        // Back To Top Fade
        if ($(this).scrollTop() > 500) {
            $('.back-to-top').fadeIn();
        } else {
            $('.back-to-top').fadeOut();
        }
  

    });

  // Smooth Scroll
  $('.navbar-nav li a,.home-buttons a,a.down,a.menu-button').bind('click',function(event){
        var $anchor = $(this);
 
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1200,'easeInOutExpo');
        
        event.preventDefault();
    });

  // Back To Top When Clicked
  $('.back-to-top').click(function () {
    $('html, body').animate({
        scrollTop: 0,
          easing: 'swing'
      }, 750);
     return false;
    });

  // Wow js and Animations
  wow = new WOW(
      {
        mobile:false
      }
    );
    wow.init();



    // Counter Up
    $('.counter').counterUp({
                delay: 10,
                time: 1000
            });


    // Fitvids Video
    $(".video").fitVids();

    // Show Tabs
    $('.eg-tab a').click(function (event) {
        event.preventDefault()
        $(this).tab('show')
    });

    // Nivo Ligthbox
    $('#shots a').nivoLightbox();

    /* Owl Carousel */

    // Feedbacks
    $("#feedbacks").owlCarousel({
        slideSpeed: 800,
        paginationSpeed: 400,
        autoPlay: 5000,
        singleItem: true,
        itemsMobile : [479, 1]
    });

    // Owl Slider
    $('#owl-slider').owlCarousel({
        pagination:false,
        slideSpeed: 800,
        autoPlay: 5000,
        singleItem: true
     });

    // Phone Slider Navigation
    $(".next").click(function(){
    $('#owl-slider').trigger('owl.next');
    });
    $(".prev").click(function(){
    $('#owl-slider').trigger('owl.prev');
    });

    // Features
    $("#owl-features").owlCarousel({
      autoPlay: false, 
      singleItem : true,
      pagination : true,
      responsive : true,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
    });


    // Showcase (Screenshots)
    $("#shots").owlCarousel({
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3],
    itemsTablet :[600,1],
    itemsMobile : [290,1]
    });

    // Email Validation Function
    function validateEmail(email) {
      var pattern= new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
      return pattern.test(email);
    };

     // Contact Form Ajax
    $('#contact-form').submit(function(event){
      event.preventDefault();
      var name = $.trim($('#contact-form #name').val()),
      email = $.trim($('#contact-form #email').val()),
      message = $.trim($('#contact-form #message').val()),
      data = 'name=' + name + '&email=' + email+ '&message=' + message;

      if(name === '' || !validateEmail(email) || message === '' ){
        $('.contact-message p').html('Please enter valid informations');
      }else{
          $.ajax({
            type: "POST",
            url: "php/sendmail.php",
            data: data,
            success: function () {
                 $('.contact-message p').html('Your message was sent successfully,thank you!');
                 $("#name").val("");
                 $("#email").val("");
                 $("#message").val("");
                
            }
        });
      }
      return false;
    });

    

    // Subscribe Form
    $('#subscribe-form').submit(function() {
    
      var email = $.trim($('#subscribe-form #email').val()),
        data= 'email=' + email;
         if(!validateEmail(email)){
        $('.subscribe-message p').html('Please enter valid informations');
      }else{
         $.ajax({
            type: "POST",
            url: "php/send-subscribe.php",
            data: data,
            success: function () {
                $('.subscribe-message p').html('Your email address was added to our subscribe list,thank you!');
                $('#email').val("");
            }
        });
      }
      return false;
    });

     // Accordion Toggle Icons Function
      function toggleicon(event) {
        $(event.target)
        .prev('.panel-heading')
        .find("i")
        .toggleClass('fa fa-arrow-circle-down fa fa-arrow-circle-up');
      }

      $('#accordion').on('hidden.bs.collapse', toggleicon);
      $('#accordion').on('shown.bs.collapse', toggleicon);
      $('#accordion2').on('hidden.bs.collapse', toggleicon);
      $('#accordion2').on('shown.bs.collapse', toggleicon);

      $('#accordion').on('show.bs.collapse', function () {
      $('#accordion .in').collapse('hide');
      });

      
      // Background Slider
      $("#home").backstretch([
        "images/backgrounds/home-background-2.jpg", 
        "images/backgrounds/home-background-3.jpg", 
        "images/backgrounds/home-background-1.jpg"
    ],{duration: 3000, fade: 750});
});








 